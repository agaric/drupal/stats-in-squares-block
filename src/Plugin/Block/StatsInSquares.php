<?php

namespace Drupal\stats_in_squares_block\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Block\BlockBase;

/**
 * Provides the Impact Block.
 *
 * @Block(
 *   id = "stats_in_squares_block",
 *   admin_label = @Translation("Stats in squares block"),
 * )
 */
class StatsInSquares extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {

    return [
      'body' => '',
   ];
  }

  /**
   * @inheritDoc
   */
  public function build() {
    $config = $this->getConfiguration();
    return [
      '#theme' => 'stats_in_squares_block',
      '#info' => $config,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $form['body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $this->t($config['body']),
    ];
    $sections = 3;
    for ($i = 0; $i <= $sections; $i++) {
      $title = isset($config['item'][$i]['title']) ? $config['item'][$i]['title'] : '';
      $number = isset($config['item'][$i]['number']) ? $config['item'][$i]['number'] : '';
      $description = isset($config['item'][$i]['description']) ? $config['item'][$i]['description'] : '';
      $form['impact'][$i]['statistics'] = [
        '#type' => 'fieldset',
        '#title' => 'Statistic ',
      ];
      $form['impact'][$i]['statistics']['title'] = [
        '#type' => 'textfield',
        '#default_value' => $this->t($title),
        '#title' => $this->t('Title'),
      ];
      $form['impact'][$i]['statistics']['number'] = [
        '#type' => 'textfield',
        '#default_value' => $this->t($number),
        '#title' => $this->t('Number'),
      ];
      $form['impact'][$i]['statistics']['description'] = [
        '#type' => 'textfield',
        '#default_value' => $this->t($description),
        '#title' => $this->t('Description'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValue('impact');
    $this->configuration['body'] = $form_state->getValue('body');
    foreach ($values as $key => $value) {
      $this->configuration['item'][$key]['title'] = $value['statistics']['title'];
      $this->configuration['item'][$key]['number'] = $value['statistics']['number'];
      $this->configuration['item'][$key]['description'] = $value['statistics']['description'];
    }
  }

}
